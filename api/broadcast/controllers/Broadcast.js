'use strict';

/**
 * Broadcast.js controller
 *
 * @description: A set of functions called "actions" for managing `Broadcast`.
 */

module.exports = {

  /**
   * Retrieve broadcast records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, { populate } = {}) => {
    if (ctx.query._q) {
      return strapi.services.broadcast.search(ctx.query);
    } else {
      return strapi.services.broadcast.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a broadcast record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.broadcast.fetch(ctx.params);
  },

  /**
   * Count broadcast records.
   *
   * @return {Number}
   */

  count: async (ctx, next, { populate } = {}) => {
    return strapi.services.broadcast.count(ctx.query, populate);
  },

  /**
   * Create a/an broadcast record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.broadcast.add(ctx.request.body);
  },

  /**
   * Update a/an broadcast record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.broadcast.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an broadcast record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.broadcast.remove(ctx.params);
  }
};
