'use strict';

/**
 * Iku.js controller
 *
 * @description: A set of functions called "actions" for managing `Iku`.
 */

module.exports = {

  /**
   * Retrieve iku records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, { populate } = {}) => {
    if (ctx.query._q) {
      return strapi.services.iku.search(ctx.query);
    } else {
      return strapi.services.iku.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a iku record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.iku.fetch(ctx.params);
  },

  /**
   * Count iku records.
   *
   * @return {Number}
   */

  count: async (ctx, next, { populate } = {}) => {
    return strapi.services.iku.count(ctx.query, populate);
  },

  /**
   * Create a/an iku record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.iku.add(ctx.request.body);
  },

  /**
   * Update a/an iku record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.iku.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an iku record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.iku.remove(ctx.params);
  }
};
