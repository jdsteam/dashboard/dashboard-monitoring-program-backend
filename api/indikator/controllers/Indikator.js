'use strict';

/**
 * Indikator.js controller
 *
 * @description: A set of functions called "actions" for managing `Indikator`.
 */

module.exports = {

  /**
   * Retrieve indikator records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, { populate } = {}) => {
    if (ctx.query._q) {
      return strapi.services.indikator.search(ctx.query);
    } else {
      return strapi.services.indikator.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a indikator record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.indikator.fetch(ctx.params);
  },

  /**
   * Count indikator records.
   *
   * @return {Number}
   */

  count: async (ctx, next, { populate } = {}) => {
    return strapi.services.indikator.count(ctx.query, populate);
  },

  /**
   * Create a/an indikator record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.indikator.add(ctx.request.body);
  },

  /**
   * Update a/an indikator record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.indikator.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an indikator record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.indikator.remove(ctx.params);
  }
};
