'use strict';

/**
 * Lapanggaran.js controller
 *
 * @description: A set of functions called "actions" for managing `Lapanggaran`.
 */

module.exports = {

  /**
   * Retrieve lapanggaran records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, { populate } = {}) => {
    if (ctx.query._q) {
      return strapi.services.lapanggaran.search(ctx.query);
    } else {
      return strapi.services.lapanggaran.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a lapanggaran record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.lapanggaran.fetch(ctx.params);
  },

  /**
   * Count lapanggaran records.
   *
   * @return {Number}
   */

  count: async (ctx, next, { populate } = {}) => {
    return strapi.services.lapanggaran.count(ctx.query, populate);
  },

  /**
   * Create a/an lapanggaran record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.lapanggaran.add(ctx.request.body);
  },

  /**
   * Update a/an lapanggaran record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.lapanggaran.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an lapanggaran record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.lapanggaran.remove(ctx.params);
  }
};
