'use strict';

/**
 * Lapanggaranplan.js controller
 *
 * @description: A set of functions called "actions" for managing `Lapanggaranplan`.
 */

module.exports = {

  /**
   * Retrieve lapanggaranplan records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, { populate } = {}) => {
    if (ctx.query._q) {
      return strapi.services.lapanggaranplan.search(ctx.query);
    } else {
      return strapi.services.lapanggaranplan.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a lapanggaranplan record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.lapanggaranplan.fetch(ctx.params);
  },

  /**
   * Count lapanggaranplan records.
   *
   * @return {Number}
   */

  count: async (ctx, next, { populate } = {}) => {
    return strapi.services.lapanggaranplan.count(ctx.query, populate);
  },

  /**
   * Create a/an lapanggaranplan record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.lapanggaranplan.add(ctx.request.body);
  },

  /**
   * Update a/an lapanggaranplan record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.lapanggaranplan.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an lapanggaranplan record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.lapanggaranplan.remove(ctx.params);
  }
};
