'use strict';

/**
 * Lapprogress.js controller
 *
 * @description: A set of functions called "actions" for managing `Lapprogress`.
 */

module.exports = {

  /**
   * Retrieve lapprogress records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, { populate } = {}) => {
    if (ctx.query._q) {
      return strapi.services.lapprogress.search(ctx.query);
    } else {
      return strapi.services.lapprogress.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a lapprogress record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.lapprogress.fetch(ctx.params);
  },

  /**
   * Count lapprogress records.
   *
   * @return {Number}
   */

  count: async (ctx, next, { populate } = {}) => {
    return strapi.services.lapprogress.count(ctx.query, populate);
  },

  /**
   * Create a/an lapprogress record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.lapprogress.add(ctx.request.body);
  },

  /**
   * Update a/an lapprogress record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.lapprogress.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an lapprogress record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.lapprogress.remove(ctx.params);
  }
};
