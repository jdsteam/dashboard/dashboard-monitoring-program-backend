'use strict';

/**
 * Log.js controller
 *
 * @description: A set of functions called "actions" for managing `Log`.
 */

module.exports = {

  /**
   * Retrieve log records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, { populate } = {}) => {
    if (ctx.query._q) {
      return strapi.services.log.search(ctx.query);
    } else {
      return strapi.services.log.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a log record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.log.fetch(ctx.params);
  },

  /**
   * Count log records.
   *
   * @return {Number}
   */

  count: async (ctx, next, { populate } = {}) => {
    return strapi.services.log.count(ctx.query, populate);
  },

  /**
   * Create a/an log record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.log.add(ctx.request.body);
  },

  /**
   * Update a/an log record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.log.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an log record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.log.remove(ctx.params);
  }
};
