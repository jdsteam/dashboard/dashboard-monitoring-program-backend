'use strict';

/**
 * Periode.js controller
 *
 * @description: A set of functions called "actions" for managing `Periode`.
 */

module.exports = {

  /**
   * Retrieve periode records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, { populate } = {}) => {
    if (ctx.query._q) {
      return strapi.services.periode.search(ctx.query);
    } else {
      return strapi.services.periode.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a periode record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.periode.fetch(ctx.params);
  },

  /**
   * Count periode records.
   *
   * @return {Number}
   */

  count: async (ctx, next, { populate } = {}) => {
    return strapi.services.periode.count(ctx.query, populate);
  },

  /**
   * Create a/an periode record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.periode.add(ctx.request.body);
  },

  /**
   * Update a/an periode record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.periode.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an periode record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.periode.remove(ctx.params);
  }
};
