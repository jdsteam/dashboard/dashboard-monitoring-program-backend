'use strict';

/**
 * Periodeanggaran.js controller
 *
 * @description: A set of functions called "actions" for managing `Periodeanggaran`.
 */

module.exports = {

  /**
   * Retrieve periodeanggaran records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, { populate } = {}) => {
    if (ctx.query._q) {
      return strapi.services.periodeanggaran.search(ctx.query);
    } else {
      return strapi.services.periodeanggaran.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a periodeanggaran record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.periodeanggaran.fetch(ctx.params);
  },

  /**
   * Count periodeanggaran records.
   *
   * @return {Number}
   */

  count: async (ctx, next, { populate } = {}) => {
    return strapi.services.periodeanggaran.count(ctx.query, populate);
  },

  /**
   * Create a/an periodeanggaran record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.periodeanggaran.add(ctx.request.body);
  },

  /**
   * Update a/an periodeanggaran record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.periodeanggaran.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an periodeanggaran record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.periodeanggaran.remove(ctx.params);
  }
};
