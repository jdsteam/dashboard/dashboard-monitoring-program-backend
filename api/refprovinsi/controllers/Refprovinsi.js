'use strict';

/**
 * Refprovinsi.js controller
 *
 * @description: A set of functions called "actions" for managing `Refprovinsi`.
 */

module.exports = {

  /**
   * Retrieve refprovinsi records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, { populate } = {}) => {
    if (ctx.query._q) {
      return strapi.services.refprovinsi.search(ctx.query);
    } else {
      return strapi.services.refprovinsi.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a refprovinsi record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.refprovinsi.fetch(ctx.params);
  },

  /**
   * Count refprovinsi records.
   *
   * @return {Number}
   */

  count: async (ctx, next, { populate } = {}) => {
    return strapi.services.refprovinsi.count(ctx.query, populate);
  },

  /**
   * Create a/an refprovinsi record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.refprovinsi.add(ctx.request.body);
  },

  /**
   * Update a/an refprovinsi record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.refprovinsi.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an refprovinsi record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.refprovinsi.remove(ctx.params);
  }
};
