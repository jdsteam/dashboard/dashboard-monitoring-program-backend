/*
 Navicat Premium Data Transfer

 Source Server         : MySQL Local
 Source Server Type    : MySQL
 Source Server Version : 100139
 Source Host           : localhost:3306
 Source Schema         : basoaciakang

 Target Server Type    : MySQL
 Target Server Version : 100139
 File Encoding         : 65001

 Date: 01/07/2019 04:48:11
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for broadcast
-- ----------------------------
DROP TABLE IF EXISTS `broadcast`;
CREATE TABLE `broadcast`  (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `to` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `message` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `cuid` int(4) NULL DEFAULT NULL,
  `cdate` datetime(0) NULL DEFAULT NULL,
  `muid` int(4) NULL DEFAULT NULL,
  `mdate` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 17 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of broadcast
-- ----------------------------
INSERT INTO `broadcast` VALUES (1, 'CARA BEDA MAKAN ACI 1', '- all - ', NULL, NULL, 1, '2019-05-15 15:03:14', 1, '2019-05-17 03:14:02');
INSERT INTO `broadcast` VALUES (2, 'CARA BEDA MAKAN ACI 2', '- all - ', NULL, NULL, 1, '2019-05-15 15:03:14', 1, '2019-05-17 03:14:02');
INSERT INTO `broadcast` VALUES (3, 'CARA BEDA MAKAN ACI 3', '- all - ', NULL, NULL, 1, '2019-05-15 15:03:14', 1, '2019-05-17 03:14:02');
INSERT INTO `broadcast` VALUES (4, 'CARA BEDA MAKAN ACI 4', '- all - ', NULL, NULL, 1, '2019-05-15 15:03:14', 1, '2019-05-17 03:13:57');

-- ----------------------------
-- Table structure for kemitraan
-- ----------------------------
DROP TABLE IF EXISTS `kemitraan`;
CREATE TABLE `kemitraan`  (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `notelp` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `domisili` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `pekerjaan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `lokasi_usaha` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `harapan_penghasilan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `pernah_usaha_kuliner` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `pernah_mitra_sejenis` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `cuid` int(4) NULL DEFAULT NULL,
  `cdate` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of kemitraan
-- ----------------------------
INSERT INTO `kemitraan` VALUES (1, 'Gelar Aditya Pratama', 'akatgelar@gmail.com', '081', 'bandung', NULL, NULL, NULL, NULL, NULL, 0, '2019-05-22 05:53:04');

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu`  (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `gambar` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `judul` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `deskripsi` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `is_active` varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  `cuid` int(4) NULL DEFAULT NULL,
  `cdate` datetime(0) NULL DEFAULT NULL,
  `muid` int(4) NULL DEFAULT NULL,
  `mdate` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 16 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES (1, 'menu1.png', 'PAKET KAROHAL', 'Baso Aci, Batagor Mini Crispy, Batagor Kering, Cuankie Kembung, Cuankie Segitiga & Tetelan Sapi', NULL, '1', 1, '2019-05-15 15:03:14', 1, '2019-05-17 03:14:02');
INSERT INTO `menu` VALUES (2, 'menu2.png', 'PAKET LAPAR', 'Baso Aci, Batagor Mini Crispy, Batagor Kering, Cuankie Kembung & Cuankie Segitiga', NULL, '1', 1, '2019-05-15 15:03:14', 1, '2019-05-17 03:14:02');
INSERT INTO `menu` VALUES (3, 'menu3.png', 'PAKET OLAB', 'Baso Aci, Batagor Mini Crispy, Batagor Kering, Cuankie Segitiga, Ceker Ayam & Tetelan', NULL, '1', 1, '2019-05-15 15:03:14', 1, '2019-05-17 03:14:02');
INSERT INTO `menu` VALUES (4, 'menu4.png', 'PAKET SALATRI', 'Baso Aci, Batagor Mini Crispy, Batagor Kering, Cuankie Kembung, Cuankie Segitiga & Ceker Ayam', NULL, '1', 1, '2019-05-15 15:03:14', 1, '2019-05-17 03:13:57');

-- ----------------------------
-- Table structure for setting
-- ----------------------------
DROP TABLE IF EXISTS `setting`;
CREATE TABLE `setting`  (
  `id` smallint(4) NOT NULL AUTO_INCREMENT,
  `kode` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `value` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `post_date` datetime(0) NULL DEFAULT NULL,
  `cuid` smallint(4) NULL DEFAULT NULL,
  `cdate` datetime(0) NULL DEFAULT NULL,
  `muid` smallint(4) NULL DEFAULT NULL,
  `mdate` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`, `kode`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 26 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of setting
-- ----------------------------
INSERT INTO `setting` VALUES (9, 'title', 'BASO ACI AKANG', '', '2019-05-15 09:18:09', 1, '2019-05-15 09:18:09', NULL, NULL);
INSERT INTO `setting` VALUES (11, 'nama', 'Baso Aci Akang', '', '2019-05-15 09:18:09', 1, '2019-05-15 09:18:09', NULL, NULL);
INSERT INTO `setting` VALUES (13, 'notelp', '0812 8484 1012', '', '2019-05-15 09:18:09', 1, '2019-05-15 09:18:09', NULL, NULL);
INSERT INTO `setting` VALUES (15, 'instagram', 'https://www.instagram.com/basoaciakang', '', '2019-05-15 09:18:09', 1, '2019-05-15 09:18:09', NULL, NULL);
INSERT INTO `setting` VALUES (12, 'alamat', 'Jl. Arafah IV Blok G 2 No. 30,<br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Komplek Islamic Village Kota Tangerang', '', '2019-05-15 09:18:09', 1, '2019-05-15 09:18:09', NULL, '2019-05-18 09:05:27');
INSERT INTO `setting` VALUES (14, 'email', 'basoaciakang.info@gmail.com', '', '2019-05-15 09:18:09', 1, '2019-05-15 09:18:09', NULL, NULL);
INSERT INTO `setting` VALUES (19, 'embed_map', 'https://www.google.com/maps/d/u/2/embed?mid=14_JlRA6p8StdBhWVLAS8ju-BV6RNpa1y', '', '2019-05-15 09:18:09', 1, '2019-05-15 09:18:09', NULL, NULL);
INSERT INTO `setting` VALUES (20, 'embed_instagram', '//lightwidget.com/widgets/476935879b6e5347944f0a55bb754327.html', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `setting` VALUES (18, 'deskripsi_profil', '<p style=\"font-size:11pt; color:#fff;  font-family: \'Lato\', sans-serif;  line-height: 1.6; text-align: justify;\">Baso Aci Akang berdiri pada bulan Januari tahun 2018. Sejak awal, kami melakukan inovasi dengan cara mengemas makanan yang paling sering di konsumsi oleh konsumen di Indonesia yaitu baso. Kami membuat baso menjadi lebih sehat karena tidak menggunakan MSG\r\n                        <br><br>\r\n                        Hingga saat ini Baso Aci Akang tercatat telah memiliki 27 Cabang di beberapa Kota Besar di Indonesia dan terus bertambah.</p>', '', '2019-05-15 09:18:09', 1, '2019-05-15 09:18:09', NULL, NULL);
INSERT INTO `setting` VALUES (22, 'deskripsi_our_journey', '<p>2007</p>\r\n            <p>We started to pay attention to the world of nursing standard. The thing that make us began was because our concern for Indonesian nurses abroad salary was very low. We all realize that because Indonesia does not yet have international license in the world for nursing.</p>\r\n            <br>\r\n            <p>2011</p>\r\n            <p>Our networking with the philippines to make a benchmark for NERS competency education system which includes the competencies achieved in the Professional Profession Program namely implementing nursing care in 11 fields of nursing namely Medical Nursing, Surgical Nursing, Maternity Nursing, Child Nursing, Critical Nursing, Life Nursing, Community Nursing, Family Nursing, Gerontik Nursing and Nursing Management, and one excellent nursing field, Palliative Nursing and Mentoring for the Sick.\r\n            </p>\r\n            <br>\r\n            <p>2017</p>\r\n            <p>With the aim of becoming an official review provider for Indonesian nurses / health care to take international examinations, PRADAYA then worked closely with the Indonesian Ministry of Health to facilitate this program.\r\n            </p>\r\n            <br>\r\n            <p>2018</p>\r\n            <p>Pradaya is officially established and become Top Rank branch in Indonesia and also will open branches in varios major cities in indonesia..\r\n            </p>', '', '2019-05-15 09:18:09', 1, '2019-05-15 09:18:09', NULL, NULL);
INSERT INTO `setting` VALUES (23, 'deskripsi_program', '<p>Pradaya Provide Community Health Education Emergency Rescue Services (CHEERS) that registered association based in Quezon City, Manila and provides trainings on Emergency Medical Services with International and Local Certification. It has modern and fully-equipped facilities and upgraded training equipment which conforms with the International Standards on Health and Safety trainings and continuously updates its technology and services through innovations and current researches.</p>\r\n            <br>\r\n            <p>CHEERS is an American Heart Association (AHA) International Training Center, an independent American Safety and Health Institute (ASHI) authorized Training Center and an Emergency Care and Safety Institute (ECSI) Educational Center in the Philippines to conduct CPR and ECC Courses for healthcare providers and lay rescuers.</p>\r\n            <br>\r\n            <h4 style=\"color:#009444; font-weight: 600;\">The Department of Health (DOH) recognizes the :</h4>\r\n            <p>Qualifications of the said International Organizations and Local accreditation from the Technical Education and Skills</p>\r\n            <br>\r\n            <h4 style=\"color:#009444; font-weight: 600;\">Development Authority (TESDA)</h4>\r\n            <p>The DOH has endorsed CHEERS to conduct trainings on hospitals, colleges, universities, LGUs and NGOs using International Certification of AHA, ASHI and ECSI..</p>\r\n            <br><br>', '', '2019-05-15 09:18:09', 1, '2019-05-15 09:18:09', NULL, NULL);
INSERT INTO `setting` VALUES (10, 'tagline', 'Cara Beda Makan Aci', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `setting` VALUES (16, 'facebook', 'https://www.facebook.com/basoaciakang', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `setting` VALUES (17, 'copyright', '© Copyright 2019 Baso Aci Akang. All Rights Reserved', NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for slider
-- ----------------------------
DROP TABLE IF EXISTS `slider`;
CREATE TABLE `slider`  (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `gambar` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `judul` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `is_active` varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  `cuid` int(4) NULL DEFAULT NULL,
  `cdate` datetime(0) NULL DEFAULT NULL,
  `muid` int(4) NULL DEFAULT NULL,
  `mdate` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 16 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of slider
-- ----------------------------
INSERT INTO `slider` VALUES (1, 'newslide1.png', 'CARA BEDA MAKAN ACI ', NULL, '1', 1, '2019-05-15 15:03:14', 1, '2019-05-17 03:14:02');
INSERT INTO `slider` VALUES (2, 'newslide1.png', 'CARA BEDA MAKAN ACI ', NULL, '1', 1, '2019-05-15 15:03:14', 1, '2019-05-17 03:14:02');
INSERT INTO `slider` VALUES (3, 'newslide1.png', 'CARA BEDA MAKAN ACI ', NULL, '0', 1, '2019-05-15 15:03:14', 1, '2019-05-17 03:14:02');
INSERT INTO `slider` VALUES (4, 'newslide1.png', 'CARA BEDA MAKAN ACI ', NULL, '0', 1, '2019-05-15 15:03:14', 1, '2019-07-01 04:46:57');

-- ----------------------------
-- Table structure for subscribe
-- ----------------------------
DROP TABLE IF EXISTS `subscribe`;
CREATE TABLE `subscribe`  (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `cuid` int(4) NULL DEFAULT NULL,
  `cdate` datetime(0) NULL DEFAULT NULL,
  `muid` int(4) NULL DEFAULT NULL,
  `mdate` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 15 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of subscribe
-- ----------------------------
INSERT INTO `subscribe` VALUES (13, 'akatgelar@gmail.com', 0, '2019-05-30 13:01:59', NULL, NULL);
INSERT INTO `subscribe` VALUES (14, 'admin@email.com', 0, '2019-06-30 20:53:55', NULL, NULL);

-- ----------------------------
-- Table structure for toping
-- ----------------------------
DROP TABLE IF EXISTS `toping`;
CREATE TABLE `toping`  (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `gambar` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `judul` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `is_active` varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  `cuid` int(4) NULL DEFAULT NULL,
  `cdate` datetime(0) NULL DEFAULT NULL,
  `muid` int(4) NULL DEFAULT NULL,
  `mdate` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 16 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of toping
-- ----------------------------
INSERT INTO `toping` VALUES (1, 'topping1.png', 'SIOMAY KEMBUNG', NULL, '1', 1, '2019-05-15 15:03:14', NULL, NULL);
INSERT INTO `toping` VALUES (2, 'topping2.png', 'CUANKIE TAHU', NULL, '1', 1, '2019-05-15 15:03:14', NULL, NULL);
INSERT INTO `toping` VALUES (3, 'topping3.png', 'CUANKIE SIOMAY', NULL, '1', 1, '2019-05-15 15:03:14', 1, '2019-05-17 03:14:02');
INSERT INTO `toping` VALUES (4, 'topping4.png', 'BATAGOR MINI', NULL, '1', 1, '2019-05-15 15:03:14', 1, '2019-05-17 03:13:57');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` smallint(4) NOT NULL AUTO_INCREMENT,
  `level_id` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `username` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password_plan` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `foto` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `last_login` datetime(0) NOT NULL,
  `cuid` smallint(4) NULL DEFAULT NULL,
  `cdate` datetime(0) NULL DEFAULT NULL,
  `muid` int(4) NULL DEFAULT NULL,
  `mdate` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`, `level_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 10 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'ADM', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', 'user_2019.05.18-04.19.27-.jpg', '0000-00-00 00:00:00', 1, '2015-08-16 14:35:31', 1, '2019-05-18 09:20:48');
INSERT INTO `user` VALUES (5, 'USER', 'user', 'ee11cbb19052e40b07aac0ca060c23ee', 'user', 'Profile_2015.08.16-09.41.20-.png', '0000-00-00 00:00:00', 5, '2015-08-16 14:41:20', NULL, NULL);

-- ----------------------------
-- Table structure for user_level
-- ----------------------------
DROP TABLE IF EXISTS `user_level`;
CREATE TABLE `user_level`  (
  `id` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama` varchar(32) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `dashboard` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '00000',
  `slider` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '00000',
  `menu` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '00000',
  `toping` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '00000',
  `video` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '00000',
  `kemitraan` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '00000',
  `subscribe` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '00000',
  `broadcast` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '00000',
  `setting` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '00000',
  `profile` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '00000',
  `user` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '00000',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_level
-- ----------------------------
INSERT INTO `user_level` VALUES ('ADM', 'Super Admin', '11111', '11111', '11111', '11111', '11111', '11111', '11111', '11111', '11111', '11111', '11111');
INSERT INTO `user_level` VALUES ('USER', 'User', '11111', '11111', '11111', '11111', '11111', '11111', '11111', '11111', '11111', '11111', '00000');

-- ----------------------------
-- Table structure for user_token
-- ----------------------------
DROP TABLE IF EXISTS `user_token`;
CREATE TABLE `user_token`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(32) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `datetime` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 538 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_token
-- ----------------------------
INSERT INTO `user_token` VALUES (537, '89325dcd9c8b399a78a240f95cf74eae', 1, '2019-07-01 04:34:19');
INSERT INTO `user_token` VALUES (536, 'acc053f76d01441a2b1c8208cdb31dc3', 5, '2019-07-01 03:37:32');
INSERT INTO `user_token` VALUES (394, '6489fc4116436e5e6b121da5df95e035', 6, '2015-05-01 22:46:08');
INSERT INTO `user_token` VALUES (460, '0471a72cacb7b1f4ef0f7a39b7f6addf', 7, '2015-08-21 02:39:12');

-- ----------------------------
-- Table structure for video
-- ----------------------------
DROP TABLE IF EXISTS `video`;
CREATE TABLE `video`  (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `judul` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `link` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `is_active` varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  `cuid` int(4) NULL DEFAULT NULL,
  `cdate` datetime(0) NULL DEFAULT NULL,
  `muid` int(4) NULL DEFAULT NULL,
  `mdate` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 16 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of video
-- ----------------------------
INSERT INTO `video` VALUES (1, '1', 'https://www.youtube.com/embed/b-zeh1Owl64', NULL, '1', 1, '2019-05-15 15:03:14', 1, '2019-05-17 03:14:02');
INSERT INTO `video` VALUES (2, '2', 'https://www.youtube.com/embed/--C_vG8PPn0', NULL, '1', 1, '2019-05-15 15:03:14', 1, '2019-05-17 03:14:02');
INSERT INTO `video` VALUES (3, '3', 'https://www.youtube.com/embed/b-zeh1Owl64', NULL, '0', 1, '2019-05-15 15:03:14', 1, '2019-05-17 03:14:02');
INSERT INTO `video` VALUES (4, '4', 'https://www.youtube.com/embed/--C_vG8PPn0', NULL, '0', 1, '2019-05-15 15:03:14', 1, '2019-05-17 03:13:57');

SET FOREIGN_KEY_CHECKS = 1;
