<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends My_Controller {

	
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('mdl_setting', 'mdl_setting'); 
		$this->load->model('mdl_register', 'mdl_register');
	}

	public function index(){

		$this->open(); 

		$data['alert'] = $this->session->flashdata('alert');
		
		$data['setting'] = $this->mdl_setting->get_setting(); 

		$this->load->view('register/register_list', $data);
		 
	}


	public function insert()
	{
		$config['upload_path'] =  $_SERVER['DOCUMENT_ROOT'].'/pradaya-frontend/assets/upload/register/';
		//$config['upload_path'] =  $_SERVER['DOCUMENT_ROOT'].'frontend/assets/upload/register/';
		$config['allowed_types'] = '*';
		$config['max_size']  = '10000'; 
		$this->load->library('upload', $config);

		$tz_string = "Asia/Jakarta"; // Use one from list of TZ names http://php.net/manual/en/timezones.php 
		$tz_object = new DateTimeZone($tz_string); 
		$datetime = new DateTime(); 
		$datetime->setTimezone($tz_object);  
			

		$this->upload->do_upload('attach_passport');
		$upload_data = $this->upload->data(); 
		$attach_passport1 =  $upload_data['file_name'];
		$attach_passport2 =  'attach_passport_'. date("Y.m.d") . '-' . date("H.i.s") .'-'.  $upload_data['file_ext'];
		rename($config['upload_path'] . $attach_passport1, $config['upload_path'] . $attach_passport2);
		
		$this->upload->do_upload('attach_diploma');
		$upload_data = $this->upload->data(); 
		$attach_diploma1 =  $upload_data['file_name'];
		$attach_diploma2 =  'attach_diploma_'. date("Y.m.d") . '-' . date("H.i.s") .'-'.  $upload_data['file_ext'];
		rename($config['upload_path'] . $attach_diploma1, $config['upload_path'] . $attach_diploma2);

		$this->upload->do_upload('attach_transcript');
		$upload_data = $this->upload->data(); 
		$attach_transcript1 =  $upload_data['file_name'];
		$attach_transcript2 =  'attach_transcript_'. date("Y.m.d") . '-' . date("H.i.s") .'-'.  $upload_data['file_ext'];
		rename($config['upload_path'] . $attach_transcript1, $config['upload_path'] . $attach_transcript2);

		$this->upload->do_upload('attach_lisence');
		$upload_data = $this->upload->data(); 
		$attach_lisence1 =  $upload_data['file_name'];
		$attach_lisence2 =  'attach_lisence_'. date("Y.m.d") . '-' . date("H.i.s") .'-'.  $upload_data['file_ext'];
		rename($config['upload_path'] . $attach_lisence1, $config['upload_path'] . $attach_lisence2);

		$this->upload->do_upload('attach_certificate');
		$upload_data = $this->upload->data(); 
		$attach_certificate1 =  $upload_data['file_name'];
		$attach_certificate2 =  'attach_certificate_'. date("Y.m.d") . '-' . date("H.i.s") .'-'.  $upload_data['file_ext'];
		rename($config['upload_path'] . $attach_certificate1, $config['upload_path'] . $attach_certificate2);

		$this->upload->do_upload('attach_photo');
		$upload_data = $this->upload->data(); 
		$attach_photo1 =  $upload_data['file_name'];
		$attach_photo2 =  'attach_photo_'. date("Y.m.d") . '-' . date("H.i.s") .'-'.  $upload_data['file_ext'];
		rename($config['upload_path'] . $attach_photo1, $config['upload_path'] . $attach_photo2);

		$data['last_name'] = $this->input->post('last_name');
		$data['first_name'] = $this->input->post('first_name');
		$data['middle_name'] = $this->input->post('middle_name');
		$data['date_of_birth'] = $this->input->post('date_of_birth');
		$data['country_of_birth'] = $this->input->post('country_of_birth');
		$data['passport_no'] = $this->input->post('passport_no');
		$data['nationality'] = $this->input->post('nationality');
		$data['gender'] = $this->input->post('gender');
		$data['visa_type'] = $this->input->post('visa_type');
		$data['email'] = $this->input->post('email');
		$data['address'] = $this->input->post('address');
		$data['city'] = $this->input->post('city');
		$data['post_code'] = $this->input->post('post_code');
		$data['country'] = $this->input->post('country');
		$data['phone'] = $this->input->post('phone');
		$data['potential_place'] = $this->input->post('potential_place');
		$data['application_for'] = $this->input->post('application_for');
		$data['hrd_number'] = $this->input->post('hrd_number');
		$data['phone_school'] = $this->input->post('phone_school');
		$data['attach_passport'] = $attach_passport2;
		$data['attach_diploma'] = $attach_diploma2;
		$data['attach_transcript'] = $attach_transcript2;
		$data['attach_lisence'] = $attach_lisence2;
		$data['attach_certificate'] = $attach_certificate2;
		$data['attach_photo'] = $attach_photo2;
		$data['cdate'] = $datetime->format('Y.m.d') . '-' .  $datetime->format("H.i.s"); 
		$data['cuid'] = '';  

		print_r($data);
		$this->mdl_register->insert($data); 

		$this->session->set_flashdata('alert', "Your information has been registered.");
		redirect('register');
	}
}
