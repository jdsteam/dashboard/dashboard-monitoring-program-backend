<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html lang="id">
<head>
    <!--====== Required meta tags ======-->
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--====== Title ======-->
    <title><?php echo $setting['title']; ?></title>
    <!--====== Favicon Icon ======-->
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/img/icon.png" type="image/png">
    <!--====== Bootstrap css ======-->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <!--====== Fontawesome css ======-->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-awesome.min.css" crossorigin="anonymous">
    <!--====== Slick css ======-->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/slick.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/slick-theme.css">
    <!--====== Magnific Popup css ======-->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/magnific-popup.css">
    <!--====== Animate css ======-->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/animate.min.css">
    <!--====== Default css ======-->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/default.css">
    <!--====== Style css ======-->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
    <!--====== Responsive css ======-->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/responsive.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/Montserra.css" >
    <link rel="stylesheet" href="<?php echo base_url();?>assets/menu/css/style.css">
</head>
<body>